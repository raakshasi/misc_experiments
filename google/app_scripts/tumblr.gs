// Define global variables needed to access Picasa Web Albums Data API
var NAME = 'lh2';
var SCOPE = 'http://picasaweb.google.com/data/';
var URL = "https://picasaweb.google.com/data/feed/api/user/default";
var EMAIL = 'abcxyz@tumblr.com';
var TAG = 'tumblr'

/**
 * Retrieve all photos tagged with 'tumblr.'
 **/
function postToTumblr(){
 var data = UrlFetchApp.fetch(URL + '?kind=photo&tag=' + TAG + '&prettyprint=true', googleOAuth_()).getContentText();
 var xmlOutput = Xml.parse(data, false);
 var photos = xmlOutput.getElement().getElements('entry');
 for(j in photos){
   sendTmblrEmail(photos[j]);
 }
}

/**
 * Send an email to Tumblr.
 **/
function sendTmblrEmail(photo) {
  Logger.log('Send email');
  var url = photo.getElement('content').getAttribute('src').getValue();
  var comment = photo.getElement('summary').getText();
  var pubDate = photo.getElement('published').getText();
  var pubDateAsDate = new Date(pubDate);
  var d = new Date();
  var twentyFourHrsAgo = d.setDate(d.getDate() -1);
  Logger.log(pubDateAsDate);
  Logger.log(twentyFourHrsAgo);
  if (pubDateAsDate > twentyFourHrsAgo) {
    emailImage(url, comment, pubDate, EMAIL);
  }
}

/**
 * This code fetches the photo at the given URL, then inlines it in an email
 * and sends the email to the given email address.
 **/
function emailImage(url, comment, pubDate, email) {
  var imgBlob = UrlFetchApp
                   .fetch(url)
                   .getBlob()
                   .setName("imageBlob");
  MailApp.sendEmail({
    to: email,
    subject: comment,
    htmlBody: "<img src='cid:imgLogo'>",
    inlineImages: { imgLogo: imgBlob }
  });
}

/**
 * Authenticate the user when accessing data from Google Services through UrlFetch
 * There are three URIs required to authenticate an application and obtain an access token,
 * one for each step of the OAuth process:
 * - Obtain a request token
 * - Authorize the request token
 * - Upgrade to an access token
 **/
function googleOAuth_() {
 var oAuthConfig = UrlFetchApp.addOAuthService(NAME);
 oAuthConfig.setRequestTokenUrl('https://www.google.com/accounts/OAuthGetRequestToken?scope='+SCOPE);
 oAuthConfig.setAuthorizationUrl('https://www.google.com/accounts/OAuthAuthorizeToken');
 oAuthConfig.setAccessTokenUrl('https://www.google.com/accounts/OAuthGetAccessToken');
 oAuthConfig.setConsumerKey('anonymous');
 oAuthConfig.setConsumerSecret('anonymous');
 return {oAuthServiceName:NAME, oAuthUseToken:'always'};
}
